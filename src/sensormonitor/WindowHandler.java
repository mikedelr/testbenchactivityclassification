package sensormonitor;

/**
 * Created by Michael Del Rosario on 17/01/2016.
 *
 * Class for managing the contents of a buffer that contains filtered signals that are kept for a pre-defined
 * window equal to the length of the buffer
 */
public class WindowHandler {
    protected int buffIdx;
    protected int winIdx;
    protected int oldIdx;
    protected double runningSum;
    protected double [] buffRunSum = new double[]{};
    protected int nSumLength = 0;

    public WindowHandler(){}

    public WindowHandler(int buffSize, int lengthToSum){
        buffRunSum = new double[buffSize];
        nSumLength = lengthToSum;
    }

    /**
     * calculates the running sum for a specified window length
     * @param smpCtr - number of samples processed
     * @param buffer - array containing values used to generate running sum
     * @param lengthToSum - number of samples in the buffer to add together
     * @return
     */

    protected double calcRunningSum(int smpCtr, double [] buffer, int lengthToSum){
        winIdx =  smpCtr & (buffer.length-1);
        //add new value to the running sum
        runningSum += buffer[winIdx];
        oldIdx = winIdx-lengthToSum;
        if (oldIdx < 0){
            oldIdx = buffer.length - lengthToSum + winIdx;
        }
        //subtract the oldest value from the running sum
        runningSum -= buffer[oldIdx];
        return runningSum;
    }

    /**
     * updates current running sum
     * @param currentSum - currentRunningSum
     * @param smpCtr - number of samples processed
     * @param buffer - array containing values used to generate running sum
     * @param lengthToSum - number of samples in the buffer to add together
     * @return
     */
    protected double calcRunningSum(double currentSum, int smpCtr,
                                    double [] buffer, int lengthToSum ){
        runningSum = currentSum;
        winIdx =  smpCtr & (buffer.length-1);
        //add new value to the running sum
        runningSum += buffer[winIdx];
        oldIdx = winIdx-lengthToSum;
        if (oldIdx < 0){
            oldIdx = buffer.length - lengthToSum + winIdx;
        }
        //subtract the oldest value from the running sum
        runningSum -= buffer[oldIdx];
        return runningSum;
    }

    /**
     * insert value into buffer based on number of samples
     * @param smpCtr - number of samples processed
     * @param val - value to be inserted to buffer
     * @param buffer - array containing values used to generate running sum
     */
    protected void fillBuffer(int smpCtr, double val, double [] buffer){
        buffIdx = smpCtr & (buffer.length-1);
        buffer[buffIdx]= val;
    }

    protected double updateRunningSum(int smpCtr, double val){
        buffIdx = smpCtr & (buffRunSum.length-1);
        buffRunSum[buffIdx]= val;
        winIdx =  smpCtr & (buffRunSum.length-1);
        //add new value to the running sum
        runningSum += buffRunSum[winIdx];
        oldIdx = winIdx-nSumLength;
        if (oldIdx < 0){
            oldIdx = buffRunSum.length - nSumLength + winIdx;
        }
        //subtract the oldest value from the running sum
        runningSum -= buffRunSum[oldIdx];
        return runningSum;
    }
}
