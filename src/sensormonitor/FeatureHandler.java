package sensormonitor;

/**
 * Created by Michael Del Rosario on 17/01/2016.
 */

/**
 *  Handler Class to manage access to aggregated feature values
 *  stored in a buffer
 */

public class FeatureHandler {
    protected int buffIdx;
    protected int featIdx;

    /**
     *
     * @param sCtr   - sample counter
     * @param val    - value to store in buffer
     * @param buffer - buffer to store value in
     */
    protected void fillBuffer(int sCtr, double val, double [] buffer){
        buffIdx = sCtr & (buffer.length-1);
        buffer[buffIdx]= val;
    }

    /**
     *
     * @param sCtr - sample counter
     * @param delayOffset - integer which determines how many positions
     *                    the index needs to be adjusted by
     * @param buffer - buffer from which to retrieve the feature value
     * @return
     */
    protected double getFeatureAtIdx(int sCtr, int delayOffset, double[] buffer){
        featIdx = (sCtr-delayOffset) & (buffer.length-1);
        return buffer[featIdx];
    }

    public FeatureHandler(){}

}
