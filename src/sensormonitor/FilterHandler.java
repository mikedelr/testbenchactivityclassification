package sensormonitor;

/**
 * Created by Michael Del Rosario on 17/01/2016.
 */
public class FilterHandler {
    protected double filteredVal;
    protected int buffIdx;
    protected int fwdLoops;
    protected int backPtr;
    protected int fillBuffIdx;

    public FilterHandler(){}

    protected double filterSignal(int signalCnt, double[] signalBuffer, double [] filterCoefficients){
//        Log.d(TAG,"signalBuffer.length: "+signalBuffer.length+ " filterCoefficients.length: "+ filterCoefficients.length);
        filteredVal = 0; // reset before calculating the new value
        buffIdx   = signalCnt&(signalBuffer.length-1);
        fwdLoops  = buffIdx+1;
        backPtr   = 1;
//        Log.d(TAG,"fwdLoops: "+fwdLoops);
        if (fwdLoops>filterCoefficients.length ){ // If the length of the sensor data buffer is longer than the filter
            fwdLoops=filterCoefficients.length;
        }
        for(int f=0;f<fwdLoops;f++){ // at least once
//            Log.d(TAG,"buffIdx: "+buffIdx+ " f: "+ f);
            filteredVal += signalBuffer[buffIdx] * filterCoefficients[f];
            buffIdx--;
        }
        for(int b=fwdLoops;b<filterCoefficients.length;b++){
//            Log.d(TAG,"signalBuffer.length-backPtr: "+(signalBuffer.length-backPtr)+ " b: "+ b);
            filteredVal += signalBuffer[signalBuffer.length-backPtr]* filterCoefficients[b];
            backPtr++;
        }
        return filteredVal;
    }

    /**
     *
     * @param ctr
     * @param val
     * @param buffer
     */
    protected void fillBuffer(int ctr, double val, double [] buffer){
        fillBuffIdx = ctr & (buffer.length-1);
        buffer[fillBuffIdx]= val;
    }
}
