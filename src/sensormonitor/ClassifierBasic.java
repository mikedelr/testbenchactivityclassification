package sensormonitor;

import android.Movements;

/**
 * Created by Michael Del Rosario on 31/03/2015.
 */
public class ClassifierBasic {
//    String TAG = "Classifier";
    public static final int STANDING  = Movements.STANDING;
    public static final int TRANSITION = Movements.TRANSITION;
    public static final int SEDENTARY   = Movements.SEDENTARY;
    public static final int WALKING    = Movements.WALKING;
    public static final int WALKUP     = Movements.WALKUP;
    public static final int WALKDN     = Movements.WALKDN;
    public static final int STATIONARY = Movements.STATIONARY;
    protected int state                = -1;

    public int fiveFeatures(double SumDifPre,double SumGloAccXY, double SumGloGyrXY, double SumDifIncAng,double SqSumDifAccZ){
        state = -1;
        if(SumGloGyrXY <= 21.907331){
            if(SumGloGyrXY <= 3.3668){
                if(SumDifIncAng <= 98.786483){
                    if(SqSumDifAccZ <= 3.317883){
                        state=STATIONARY;
                    }
                    if(SqSumDifAccZ > 3.317883){
                        if(SumGloAccXY <= 36.700311){
                            state=STATIONARY;
                        }
                        if(SumGloAccXY > 36.700311){
                            if(SumGloAccXY <= 881.496801){
                                if(SqSumDifAccZ <= 393.937903){
                                    state=STATIONARY;
                                }
                                if(SqSumDifAccZ > 393.937903){
                                    state=WALKING;
                                }
                            }
                            if(SumGloAccXY > 881.496801){
                                state=STATIONARY;
                            }
                        }
                    }
                }
                if(SumDifIncAng > 98.786483){
                    state=STATIONARY;
                }
            }
            if(SumGloGyrXY > 3.3668){
                if(SumDifIncAng <= 95.644196){
                    if(SumGloAccXY <= 73.181336){
                        if(SumDifIncAng <= 85.77046){
                            if(SumDifIncAng <= 70.24224){
                                state=TRANSITION;
                            }
                            if(SumDifIncAng > 70.24224){
                                state=STATIONARY;
                            }
                        }
                        if(SumDifIncAng > 85.77046){
                            state=STATIONARY;
                        }
                    }
                    if(SumGloAccXY > 73.181336){
                        state=STATIONARY;
                    }
                }
                if(SumDifIncAng > 95.644196){
                    state=STATIONARY;
                }
            }
        }
        if(SumGloGyrXY > 21.907331){
            if(SumDifIncAng <= 91.481661){
                if(SumGloAccXY <= 313.985375){
                    if(SumDifPre <= 12.285172){
                        if(SumDifIncAng <= 54.679916){
                            state=TRANSITION;
                        }
                        if(SumDifIncAng > 54.679916){
                            if(SumGloAccXY <= 148.689441){
                                if(SumDifIncAng <= 86.335362){
                                    state=TRANSITION;
                                }
                                if(SumDifIncAng > 86.335362){
                                    state=STATIONARY;
                                }
                            }
                            if(SumGloAccXY > 148.689441){
                                if(SumDifPre <= 6.515675){
                                    if(SumDifIncAng <= 83.176386){
                                        if(SqSumDifAccZ <= 40.06742){
                                            state=TRANSITION;
                                        }
                                        if(SqSumDifAccZ > 40.06742){
                                            if(SumGloAccXY <= 177.437539){
                                                state=STATIONARY;
                                            }
                                            if(SumGloAccXY > 177.437539){
                                                if(SumDifIncAng <= 76.306275){
                                                    state=TRANSITION;
                                                }
                                                if(SumDifIncAng > 76.306275){
                                                    state=STATIONARY;
                                                }
                                            }
                                        }
                                    }
                                    if(SumDifIncAng > 83.176386){
                                        if(SumDifPre <= -7.071673){
                                            state=TRANSITION;
                                        }
                                        if(SumDifPre > -7.071673){
                                            state=STATIONARY;
                                        }
                                    }
                                }
                                if(SumDifPre > 6.515675){
                                    if(SumGloGyrXY <= 78.961807){
                                        if(SumDifPre <= 8.774293){
                                            if(SumGloAccXY <= 273.847457){
                                                state=TRANSITION;
                                            }
                                            if(SumGloAccXY > 273.847457){
                                                state=WALKING;
                                            }
                                        }
                                        if(SumDifPre > 8.774293){
                                            if(SumGloGyrXY <= 65.697865){
                                                if(SumGloGyrXY <= 53.290988){
                                                    state=STATIONARY;
                                                }
                                                if(SumGloGyrXY > 53.290988){
                                                    state=TRANSITION;
                                                }
                                            }
                                            if(SumGloGyrXY > 65.697865){
                                                state=STATIONARY;
                                            }
                                        }
                                    }
                                    if(SumGloGyrXY > 78.961807){
                                        if(SqSumDifAccZ <= 58.715664){
                                            state=TRANSITION;
                                        }
                                        if(SqSumDifAccZ > 58.715664){
                                            state=WALKUP;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if(SumDifPre > 12.285172){
                        if(SumDifIncAng <= 69.757482){
                            if(SumDifIncAng <= 58.824971){
                                state=TRANSITION;
                            }
                            if(SumDifIncAng > 58.824971){
                                if(SumGloGyrXY <= 94.125264){
                                    state=TRANSITION;
                                }
                                if(SumGloGyrXY > 94.125264){
                                    state=WALKUP;
                                }
                            }
                        }
                        if(SumDifIncAng > 69.757482){
                            if(SumGloGyrXY <= 49.31843){
                                if(SumGloAccXY <= 164.48548){
                                    state=TRANSITION;
                                }
                                if(SumGloAccXY > 164.48548){
                                    if(SumGloAccXY <= 222.505702){
                                        state=STATIONARY;
                                    }
                                    if(SumGloAccXY > 222.505702){
                                        if(SumGloAccXY <= 248.95613){
                                            state=WALKING;
                                        }
                                        if(SumGloAccXY > 248.95613){
                                            state=STATIONARY;
                                        }
                                    }
                                }
                            }
                            if(SumGloGyrXY > 49.31843){
                                state=WALKUP;
                            }
                        }
                    }
                }
                if(SumGloAccXY > 313.985375){
                    if(SumDifPre <= 8.659076){
                        if(SumDifIncAng <= 62.711791){
                            if(SqSumDifAccZ <= 362.188001){
                                state=TRANSITION;
                            }
                            if(SqSumDifAccZ > 362.188001){
                                if(SumGloGyrXY <= 60.229193){
                                    state=STATIONARY;
                                }
                                if(SumGloGyrXY > 60.229193){
                                    if(SumDifIncAng <= -6.79968){
                                        if(SumDifIncAng <= -33.143841){
                                            if(SumDifPre <= -7.132462){
                                                state=TRANSITION;
                                            }
                                            if(SumDifPre > -7.132462){
                                                state=STATIONARY;
                                            }
                                        }
                                        if(SumDifIncAng > -33.143841){
                                            state=WALKING;
                                        }
                                    }
                                    if(SumDifIncAng > -6.79968){
                                        if(SumDifPre <= -6.37892){
                                            if(SumDifIncAng <= 28.098945){
                                                state=TRANSITION;
                                            }
                                            if(SumDifIncAng > 28.098945){
                                                if(SumGloAccXY <= 3491.403145){
                                                    if(SumDifPre <= -10.615306){
                                                        if(SumGloAccXY <= 2472.390717){
                                                            state=TRANSITION;
                                                        }
                                                        if(SumGloAccXY > 2472.390717){
                                                            state=WALKING;
                                                        }
                                                    }
                                                    if(SumDifPre > -10.615306){
                                                        state=STATIONARY;
                                                    }
                                                }
                                                if(SumGloAccXY > 3491.403145){
                                                    state=WALKING;
                                                }
                                            }
                                        }
                                        if(SumDifPre > -6.37892){
                                            if(SqSumDifAccZ <= 2044.330921){
                                                if(SumDifIncAng <= 18.308456){
                                                    state=TRANSITION;
                                                }
                                                if(SumDifIncAng > 18.308456){
                                                    state=WALKING;
                                                }
                                            }
                                            if(SqSumDifAccZ > 2044.330921){
                                                state=WALKING;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if(SumDifIncAng > 62.711791){
                            if(SumGloGyrXY <= 95.710416){
                                if(SqSumDifAccZ <= 50.573962){
                                    if(SumDifIncAng <= 87.093867){
                                        if(SumGloAccXY <= 1135.153478){
                                            if(SqSumDifAccZ <= 4.577599){
                                                state=TRANSITION;
                                            }
                                            if(SqSumDifAccZ > 4.577599){
                                                if(SqSumDifAccZ <= 7.615508){
                                                    state=TRANSITION;
                                                }
                                                if(SqSumDifAccZ > 7.615508){
                                                    if(SumDifIncAng <= 81.523359){
                                                        if(SumGloAccXY <= 668.056697){
                                                            state=TRANSITION;
                                                        }
                                                        if(SumGloAccXY > 668.056697){
                                                            state=STATIONARY;
                                                        }
                                                    }
                                                    if(SumDifIncAng > 81.523359){
                                                        if(SumDifIncAng <= 81.822329){
                                                            state=STATIONARY;
                                                        }
                                                        if(SumDifIncAng > 81.822329){
                                                            if(SumGloGyrXY <= 84.585024){
                                                                state=STATIONARY;
                                                            }
                                                            if(SumGloGyrXY > 84.585024){
                                                                state=WALKING;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if(SumGloAccXY > 1135.153478){
                                            if(SumGloGyrXY <= 37.541563){
                                                state=STATIONARY;
                                            }
                                            if(SumGloGyrXY > 37.541563){
                                                state=WALKING;
                                            }
                                        }
                                    }
                                    if(SumDifIncAng > 87.093867){
                                        if(SumDifPre <= -7.858796){
                                            if(SqSumDifAccZ <= 31.582086){
                                                state=WALKING;
                                            }
                                            if(SqSumDifAccZ > 31.582086){
                                                if(SumGloAccXY <= 783.575181){
                                                    state=WALKDN;
                                                }
                                                if(SumGloAccXY > 783.575181){
                                                    state=WALKING;
                                                }
                                            }
                                        }
                                        if(SumDifPre > -7.858796){
                                            state=WALKING;
                                        }
                                    }
                                }
                                if(SqSumDifAccZ > 50.573962){
                                    if(SumGloGyrXY <= 63.745037){
                                        state=STATIONARY;
                                    }
                                    if(SumGloGyrXY > 63.745037){
                                        if(SumGloAccXY <= 444.854816){
                                            state=TRANSITION;
                                        }
                                        if(SumGloAccXY > 444.854816){
                                            state=WALKING;
                                        }
                                    }
                                }
                            }
                            if(SumGloGyrXY > 95.710416){
                                if(SumDifPre <= -14.979565){
                                    state=WALKING;
                                }
                                if(SumDifPre > -14.979565){
                                    if(SumGloAccXY <= 1244.48431){
                                        if(SumDifIncAng <= 80.275282){
                                            if(SumDifPre <= 6.60055){
                                                if(SqSumDifAccZ <= 25.070477){
                                                    state=TRANSITION;
                                                }
                                                if(SqSumDifAccZ > 25.070477){
                                                    if(SumGloAccXY <= 393.298887){
                                                        state=TRANSITION;
                                                    }
                                                    if(SumGloAccXY > 393.298887){
                                                        state=WALKING;
                                                    }
                                                }
                                            }
                                            if(SumDifPre > 6.60055){
                                                state=WALKING;
                                            }
                                        }
                                        if(SumDifIncAng > 80.275282){
                                            if(SumDifPre <= 3.720591){
                                                if(SumGloAccXY <= 379.393738){
                                                    state=STATIONARY;
                                                }
                                                if(SumGloAccXY > 379.393738){
                                                    state=WALKING;
                                                }
                                            }
                                            if(SumDifPre > 3.720591){
                                                state=WALKING;
                                            }
                                        }
                                    }
                                    if(SumGloAccXY > 1244.48431){
                                        state=WALKING;
                                    }
                                }
                            }
                        }
                    }
                    if(SumDifPre > 8.659076){
                        if(SumDifIncAng <= 62.346783){
                            if(SumDifPre <= 21.427167){
                                if(SqSumDifAccZ <= 482.99019){
                                    if(SumDifIncAng <= 37.590374){
                                        state=TRANSITION;
                                    }
                                    if(SumDifIncAng > 37.590374){
                                        if(SumGloGyrXY <= 120.998418){
                                            if(SumGloGyrXY <= 111.812112){
                                                if(SqSumDifAccZ <= 26.538729){
                                                    state=TRANSITION;
                                                }
                                                if(SqSumDifAccZ > 26.538729){
                                                    if(SumDifPre <= 11.75511){
                                                        state=TRANSITION;
                                                    }
                                                    if(SumDifPre > 11.75511){
                                                        if(SumDifPre <= 14.262302){
                                                            state=WALKING;
                                                        }
                                                        if(SumDifPre > 14.262302){
                                                            state=STATIONARY;
                                                        }
                                                    }
                                                }
                                            }
                                            if(SumGloGyrXY > 111.812112){
                                                state=WALKUP;
                                            }
                                        }
                                        if(SumGloGyrXY > 120.998418){
                                            state=WALKING;
                                        }
                                    }
                                }
                                if(SqSumDifAccZ > 482.99019){
                                    state=WALKING;
                                }
                            }
                            if(SumDifPre > 21.427167){
                                if(SumGloGyrXY <= 243.119186){
                                    state=TRANSITION;
                                }
                                if(SumGloGyrXY > 243.119186){
                                    if(SumDifPre <= 32.434284){
                                        state=WALKING;
                                    }
                                    if(SumDifPre > 32.434284){
                                        state=WALKUP;
                                    }
                                }
                            }
                        }
                        if(SumDifIncAng > 62.346783){
                            if(SumGloGyrXY <= 93.435131){
                                if(SumGloAccXY <= 487.494301){
                                    if(SumDifIncAng <= 77.232614){
                                        state=TRANSITION;
                                    }
                                    if(SumDifIncAng > 77.232614){
                                        state=WALKING;
                                    }
                                }
                                if(SumGloAccXY > 487.494301){
                                    state=WALKING;
                                }
                            }
                            if(SumGloGyrXY > 93.435131){
                                if(SumGloGyrXY <= 260.170277){
                                    if(SqSumDifAccZ <= 40.565945){
                                        state=WALKING;
                                    }
                                    if(SqSumDifAccZ > 40.565945){
                                        if(SumGloAccXY <= 505.335829){
                                            if(SqSumDifAccZ <= 55.377072){
                                                if(SumDifPre <= 23.132893){
                                                    state=WALKING;
                                                }
                                                if(SumDifPre > 23.132893){
                                                    state=WALKUP;
                                                }
                                            }
                                            if(SqSumDifAccZ > 55.377072){
                                                state=WALKUP;
                                            }
                                        }
                                        if(SumGloAccXY > 505.335829){
                                            if(SumGloAccXY <= 1368.066249){
                                                state=WALKUP;
                                            }
                                            if(SumGloAccXY > 1368.066249){
                                                state=WALKING;
                                            }
                                        }
                                    }
                                }
                                if(SumGloGyrXY > 260.170277){
                                    state=WALKUP;
                                }
                            }
                        }
                    }
                }
            }
            if(SumDifIncAng > 91.481661){
                if(SumDifPre <= -13.030464){
                    if(SqSumDifAccZ <= 4.442205){
                        if(SumDifPre <= -27.830673){
                            if(SumGloAccXY <= 968.95099){
                                state=WALKDN;
                            }
                            if(SumGloAccXY > 968.95099){
                                if(SumGloGyrXY <= 85.875756){
                                    state=WALKING;
                                }
                                if(SumGloGyrXY > 85.875756){
                                    state=WALKDN;
                                }
                            }
                        }
                        if(SumDifPre > -27.830673){
                            state=WALKING;
                        }
                    }
                    if(SqSumDifAccZ > 4.442205){
                        if(SumDifPre <= -22.360314){
                            if(SumDifPre <= -43.520168){
                                if(SumGloGyrXY <= 78.675943){
                                    state=STATIONARY;
                                }
                                if(SumGloGyrXY > 78.675943){
                                    state=WALKDN;
                                }
                            }
                            if(SumDifPre > -43.520168){
                                state=WALKDN;
                            }
                        }
                        if(SumDifPre > -22.360314){
                            if(SumGloGyrXY <= 51.988347){
                                if(SumGloAccXY <= 131.722884){
                                    state=STATIONARY;
                                }
                                if(SumGloAccXY > 131.722884){
                                    if(SumDifIncAng <= 95.075174){
                                        state=STATIONARY;
                                    }
                                    if(SumDifIncAng > 95.075174){
                                        if(SqSumDifAccZ <= 9.294636){
                                            if(SumDifPre <= -17.129924){
                                                if(SumGloGyrXY <= 37.055789){
                                                    state=WALKDN;
                                                }
                                                if(SumGloGyrXY > 37.055789){
                                                    state=WALKING;
                                                }
                                            }
                                            if(SumDifPre > -17.129924){
                                                if(SumGloAccXY <= 311.883461){
                                                    state=STATIONARY;
                                                }
                                                if(SumGloAccXY > 311.883461){
                                                    state=WALKING;
                                                }
                                            }
                                        }
                                        if(SqSumDifAccZ > 9.294636){
                                            if(SumDifIncAng <= 98.588566){
                                                if(SumDifPre <= -17.816438){
                                                    state=WALKDN;
                                                }
                                                if(SumDifPre > -17.816438){
                                                    state=WALKING;
                                                }
                                            }
                                            if(SumDifIncAng > 98.588566){
                                                state=WALKDN;
                                            }
                                        }
                                    }
                                }
                            }
                            if(SumGloGyrXY > 51.988347){
                                if(SumGloAccXY <= 993.99159){
                                    if(SumDifIncAng <= 95.499941){
                                        if(SqSumDifAccZ <= 20.838122){
                                            state=WALKING;
                                        }
                                        if(SqSumDifAccZ > 20.838122){
                                            state=WALKDN;
                                        }
                                    }
                                    if(SumDifIncAng > 95.499941){
                                        state=WALKDN;
                                    }
                                }
                                if(SumGloAccXY > 993.99159){
                                    state=WALKING;
                                }
                            }
                        }
                    }
                }
                if(SumDifPre > -13.030464){
                    if(SumDifPre <= 14.127219){
                        if(SumGloAccXY <= 379.662196){
                            if(SumGloAccXY <= 203.330897){
                                if(SumGloAccXY <= 109.195677){
                                    state=STATIONARY;
                                }
                                if(SumGloAccXY > 109.195677){
                                    if(SqSumDifAccZ <= 4.341011){
                                        if(SumDifIncAng <= 97.965362){
                                            state=STATIONARY;
                                        }
                                        if(SumDifIncAng > 97.965362){
                                            state=WALKING;
                                        }
                                    }
                                    if(SqSumDifAccZ > 4.341011){
                                        state=STATIONARY;
                                    }
                                }
                            }
                            if(SumGloAccXY > 203.330897){
                                state=WALKING;
                            }
                        }
                        if(SumGloAccXY > 379.662196){
                            if(SumGloGyrXY <= 83.15176){
                                if(SumDifIncAng <= 98.518474){
                                    if(SumGloGyrXY <= 42.501639){
                                        state=WALKING;
                                    }
                                    if(SumGloGyrXY > 42.501639){
                                        if(SumDifPre <= -6.951298){
                                            if(SqSumDifAccZ <= 8.436221){
                                                state=WALKING;
                                            }
                                            if(SqSumDifAccZ > 8.436221){
                                                if(SumDifIncAng <= 92.994888){
                                                    if(SumDifPre <= -8.655473){
                                                        state=WALKDN;
                                                    }
                                                    if(SumDifPre > -8.655473){
                                                        state=TRANSITION;
                                                    }
                                                }
                                                if(SumDifIncAng > 92.994888){
                                                    state=WALKING;
                                                }
                                            }
                                        }
                                        if(SumDifPre > -6.951298){
                                            state=WALKING;
                                        }
                                    }
                                }
                                if(SumDifIncAng > 98.518474){
                                    if(SqSumDifAccZ <= 13.520501){
                                        state=WALKING;
                                    }
                                    if(SqSumDifAccZ > 13.520501){
                                        if(SumDifPre <= -6.931267){
                                            state=WALKING;
                                        }
                                        if(SumDifPre > -6.931267){
                                            if(SumGloGyrXY <= 38.466777){
                                                if(SumGloAccXY <= 898.80978){
                                                    state=WALKING;
                                                }
                                                if(SumGloAccXY > 898.80978){
                                                    state=STATIONARY;
                                                }
                                            }
                                            if(SumGloGyrXY > 38.466777){
                                                state=WALKING;
                                            }
                                        }
                                    }
                                }
                            }
                            if(SumGloGyrXY > 83.15176){
                                state=WALKING;
                            }
                        }
                    }
                    if(SumDifPre > 14.127219){
                        if(SqSumDifAccZ <= 8.78311){
                            if(SumDifPre <= 25.695551){
                                if(SumGloAccXY <= 208.27371){
                                    if(SumGloAccXY <= 188.84551){
                                        state=STATIONARY;
                                    }
                                    if(SumGloAccXY > 188.84551){
                                        state=WALKING;
                                    }
                                }
                                if(SumGloAccXY > 208.27371){
                                    state=WALKING;
                                }
                            }
                            if(SumDifPre > 25.695551){
                                if(SqSumDifAccZ <= 2.313569){
                                    state=WALKING;
                                }
                                if(SqSumDifAccZ > 2.313569){
                                    if(SumGloAccXY <= 1896.711304){
                                        state=WALKUP;
                                    }
                                    if(SumGloAccXY > 1896.711304){
                                        state=WALKING;
                                    }
                                }
                            }
                        }
                        if(SqSumDifAccZ > 8.78311){
                            if(SumGloGyrXY <= 94.74296){
                                if(SumGloGyrXY <= 61.795579){
                                    state=WALKING;
                                }
                                if(SumGloGyrXY > 61.795579){
                                    if(SumGloAccXY <= 622.326279){
                                        state=WALKUP;
                                    }
                                    if(SumGloAccXY > 622.326279){
                                        state=WALKING;
                                    }
                                }
                            }
                            if(SumGloGyrXY > 94.74296){
                                if(SumGloAccXY <= 1707.065524){
                                    state=WALKUP;
                                }
                                if(SumGloAccXY > 1707.065524){
                                    if(SumGloGyrXY <= 262.85207){
                                        state=WALKING;
                                    }
                                    if(SumGloGyrXY > 262.85207){
                                        if(SumGloAccXY <= 2174.358654){
                                            state=WALKUP;
                                        }
                                        if(SumGloAccXY > 2174.358654){
                                            if(SumGloGyrXY <= 350.166094){
                                                state=WALKING;
                                            }
                                            if(SumGloGyrXY > 350.166094){
                                                state=WALKUP;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return state;
    }

    /**
     * This is the decision tree algorithm obtained from WEKA
     * @param SumDifPre
     * @param SumSqBpfGyrXYZ
     * @param SumSqLpfDifAccXYZ
     * @return
     */
    public int classifyThreeFeaturesPMEAS(double SumDifPre, double SumSqBpfGyrXYZ, double SumSqLpfDifAccXYZ){
        if(SumSqBpfGyrXYZ <= 32.05795){
            if(SumSqLpfDifAccXYZ <= 1260.532842){
                if(SumSqBpfGyrXYZ <= 6.950407){
                    if(SumSqLpfDifAccXYZ <= 105.436218){
                        if(SumSqLpfDifAccXYZ <= 11.628291){
                            state=STATIONARY;
                        }
                        if(SumSqLpfDifAccXYZ > 11.628291){
                            if(SumSqBpfGyrXYZ <= 0.011064){
                                if(SumSqLpfDifAccXYZ <= 11.64133){
                                    state=TRANSITION;
                                }
                                if(SumSqLpfDifAccXYZ > 11.64133){
                                    if(SumSqLpfDifAccXYZ <= 22.584463){
                                        state=STATIONARY;
                                    }
                                    if(SumSqLpfDifAccXYZ > 22.584463){
                                        if(SumSqLpfDifAccXYZ <= 47.366436){
                                            if(SumSqLpfDifAccXYZ <= 42.708673){
                                                state=STATIONARY;
                                            }
                                            if(SumSqLpfDifAccXYZ > 42.708673){
                                                if(SumSqBpfGyrXYZ <= 0.008798){
                                                    state=TRANSITION;
                                                }
                                                if(SumSqBpfGyrXYZ > 0.008798){
                                                    state=STATIONARY;
                                                }
                                            }
                                        }
                                        if(SumSqLpfDifAccXYZ > 47.366436){
                                            state=STATIONARY;
                                        }
                                    }
                                }
                            }
                            if(SumSqBpfGyrXYZ > 0.011064){
                                state=STATIONARY;
                            }
                        }
                    }
                    if(SumSqLpfDifAccXYZ > 105.436218){
                        if(SumSqLpfDifAccXYZ <= 327.438337){
                            state=STATIONARY;
                        }
                        if(SumSqLpfDifAccXYZ > 327.438337){
                            if(SumDifPre <= -7.828931){
                                if(SumSqBpfGyrXYZ <= 0.124582){
                                    state=TRANSITION;
                                }
                                if(SumSqBpfGyrXYZ > 0.124582){
                                    state=STATIONARY;
                                }
                            }
                            if(SumDifPre > -7.828931){
                                state=STATIONARY;
                            }
                        }
                    }
                }
                if(SumSqBpfGyrXYZ > 6.950407){
                    if(SumSqBpfGyrXYZ <= 21.063711){
                        state=STATIONARY;
                    }
                    if(SumSqBpfGyrXYZ > 21.063711){
                        if(SumDifPre <= -8.569499){
                            if(SumSqLpfDifAccXYZ <= 106.83529){
                                if(SumDifPre <= -12.424644){
                                    state=WALKING;
                                }
                                if(SumDifPre > -12.424644){
                                    if(SumSqBpfGyrXYZ <= 28.538246){
                                        state=STATIONARY;
                                    }
                                    if(SumSqBpfGyrXYZ > 28.538246){
                                        state=WALKING;
                                    }
                                }
                            }
                            if(SumSqLpfDifAccXYZ > 106.83529){
                                state=STATIONARY;
                            }
                        }
                        if(SumDifPre > -8.569499){
                            state=STATIONARY;
                        }
                    }
                }
            }
            if(SumSqLpfDifAccXYZ > 1260.532842){
                if(SumSqLpfDifAccXYZ <= 3684.630639){
                    if(SumDifPre <= -14.339918){
                        if(SumSqBpfGyrXYZ <= 12.33917){
                            if(SumSqBpfGyrXYZ <= 1.585464){
                                if(SumSqLpfDifAccXYZ <= 1361.330739){
                                    state=TRANSITION;
                                }
                                if(SumSqLpfDifAccXYZ > 1361.330739){
                                    state=WALKING;
                                }
                            }
                            if(SumSqBpfGyrXYZ > 1.585464){
                                state=STATIONARY;
                            }
                        }
                        if(SumSqBpfGyrXYZ > 12.33917){
                            state=STATIONARY;
                        }
                    }
                    if(SumDifPre > -14.339918){
                        if(SumSqBpfGyrXYZ <= 18.244184){
                            state=STATIONARY;
                        }
                        if(SumSqBpfGyrXYZ > 18.244184){
                            if(SumSqLpfDifAccXYZ <= 2707.846348){
                                state=STATIONARY;
                            }
                            if(SumSqLpfDifAccXYZ > 2707.846348){
                                if(SumDifPre <= 6.698575){
                                    state=STATIONARY;
                                }
                                if(SumDifPre > 6.698575){
                                    if(SumSqBpfGyrXYZ <= 25.833128){
                                        if(SumSqBpfGyrXYZ <= 20.811235){
                                            state=TRANSITION;
                                        }
                                        if(SumSqBpfGyrXYZ > 20.811235){
                                            if(SumDifPre <= 18.854334){
                                                state=STATIONARY;
                                            }
                                            if(SumDifPre > 18.854334){
                                                state=WALKUP;
                                            }
                                        }
                                    }
                                    if(SumSqBpfGyrXYZ > 25.833128){
                                        state=WALKUP;
                                    }
                                }
                            }
                        }
                    }
                }
                if(SumSqLpfDifAccXYZ > 3684.630639){
                    if(SumDifPre <= 10.741055){
                        state=TRANSITION;
                    }
                    if(SumDifPre > 10.741055){
                        if(SumSqLpfDifAccXYZ <= 7495.523231){
                            if(SumSqBpfGyrXYZ <= 15.280895){
                                state=TRANSITION;
                            }
                            if(SumSqBpfGyrXYZ > 15.280895){
                                if(SumSqLpfDifAccXYZ <= 6315.158981){
                                    state=WALKUP;
                                }
                                if(SumSqLpfDifAccXYZ > 6315.158981){
                                    state=TRANSITION;
                                }
                            }
                        }
                        if(SumSqLpfDifAccXYZ > 7495.523231){
                            state=TRANSITION;
                        }
                    }
                }
            }
        }
        if(SumSqBpfGyrXYZ > 32.05795){
            if(SumSqLpfDifAccXYZ <= 149.856317){
                if(SumDifPre <= -19.137685){
                    if(SumDifPre <= -25.928234){
                        if(SumDifPre <= -56.97284){
                            state=STATIONARY;
                        }
                        if(SumDifPre > -56.97284){
                            if(SumSqLpfDifAccXYZ <= 11.396798){
                                state=WALKING;
                            }
                            if(SumSqLpfDifAccXYZ > 11.396798){
                                state=WALKDN;
                            }
                        }
                    }
                    if(SumDifPre > -25.928234){
                        state=WALKING;
                    }
                }
                if(SumDifPre > -19.137685){
                    if(SumDifPre <= 20.849264){
                        if(SumSqBpfGyrXYZ <= 60.798317){
                            state=WALKING;
                        }
                        if(SumSqBpfGyrXYZ > 60.798317){
                            if(SumDifPre <= -9.44214){
                                state=WALKING;
                            }
                            if(SumDifPre > -9.44214){
                                if(SumSqLpfDifAccXYZ <= 77.436825){
                                    state=WALKING;
                                }
                                if(SumSqLpfDifAccXYZ > 77.436825){
                                    if(SumDifPre <= 14.82775){
                                        state=WALKING;
                                    }
                                    if(SumDifPre > 14.82775){
                                        if(SumDifPre <= 17.561043){
                                            if(SumSqBpfGyrXYZ <= 124.462468){
                                                state=WALKING;
                                            }
                                            if(SumSqBpfGyrXYZ > 124.462468){
                                                if(SumSqLpfDifAccXYZ <= 113.094696){
                                                    if(SumSqLpfDifAccXYZ <= 86.641374){
                                                        state=WALKING;
                                                    }
                                                    if(SumSqLpfDifAccXYZ > 86.641374){
                                                        state=WALKUP;
                                                    }
                                                }
                                                if(SumSqLpfDifAccXYZ > 113.094696){
                                                    state=WALKING;
                                                }
                                            }
                                        }
                                        if(SumDifPre > 17.561043){
                                            state=WALKING;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if(SumDifPre > 20.849264){
                        if(SumSqLpfDifAccXYZ <= 37.930578){
                            state=WALKING;
                        }
                        if(SumSqLpfDifAccXYZ > 37.930578){
                            if(SumDifPre <= 26.690649){
                                if(SumSqBpfGyrXYZ <= 74.3492){
                                    state=STATIONARY;
                                }
                                if(SumSqBpfGyrXYZ > 74.3492){
                                    if(SumSqLpfDifAccXYZ <= 59.958712){
                                        state=WALKING;
                                    }
                                    if(SumSqLpfDifAccXYZ > 59.958712){
                                        state=WALKUP;
                                    }
                                }
                            }
                            if(SumDifPre > 26.690649){
                                state=WALKUP;
                            }
                        }
                    }
                }
            }
            if(SumSqLpfDifAccXYZ > 149.856317){
                if(SumSqLpfDifAccXYZ <= 2344.958328){
                    if(SumDifPre <= 11.475189){
                        if(SumDifPre <= -11.394788){
                            if(SumDifPre <= -22.360314){
                                state=WALKDN;
                            }
                            if(SumDifPre > -22.360314){
                                if(SumSqBpfGyrXYZ <= 126.164455){
                                    if(SumDifPre <= -17.030032){
                                        if(SumSqLpfDifAccXYZ <= 1003.760152){
                                            state=WALKDN;
                                        }
                                        if(SumSqLpfDifAccXYZ > 1003.760152){
                                            state=WALKING;
                                        }
                                    }
                                    if(SumDifPre > -17.030032){
                                        if(SumDifPre <= -13.467782){
                                            if(SumSqBpfGyrXYZ <= 112.767732){
                                                state=WALKING;
                                            }
                                            if(SumSqBpfGyrXYZ > 112.767732){
                                                if(SumSqBpfGyrXYZ <= 121.268267){
                                                    state=WALKDN;
                                                }
                                                if(SumSqBpfGyrXYZ > 121.268267){
                                                    state=WALKING;
                                                }
                                            }
                                        }
                                        if(SumDifPre > -13.467782){
                                            state=WALKING;
                                        }
                                    }
                                }
                                if(SumSqBpfGyrXYZ > 126.164455){
                                    state=WALKING;
                                }
                            }
                        }
                        if(SumDifPre > -11.394788){
                            if(SumSqBpfGyrXYZ <= 71.215928){
                                if(SumSqLpfDifAccXYZ <= 958.543151){
                                    state=WALKING;
                                }
                                if(SumSqLpfDifAccXYZ > 958.543151){
                                    if(SumSqBpfGyrXYZ <= 53.866116){
                                        if(SumDifPre <= -6.702394){
                                            state=WALKING;
                                        }
                                        if(SumDifPre > -6.702394){
                                            state=STATIONARY;
                                        }
                                    }
                                    if(SumSqBpfGyrXYZ > 53.866116){
                                        state=WALKING;
                                    }
                                }
                            }
                            if(SumSqBpfGyrXYZ > 71.215928){
                                if(SumSqBpfGyrXYZ <= 147.970147){
                                    if(SumSqLpfDifAccXYZ <= 1032.63193){
                                        state=WALKING;
                                    }
                                    if(SumSqLpfDifAccXYZ > 1032.63193){
                                        if(SumDifPre <= -6.679559){
                                            if(SumSqBpfGyrXYZ <= 129.998656){
                                                state=WALKING;
                                            }
                                            if(SumSqBpfGyrXYZ > 129.998656){
                                                state=TRANSITION;
                                            }
                                        }
                                        if(SumDifPre > -6.679559){
                                            state=WALKING;
                                        }
                                    }
                                }
                                if(SumSqBpfGyrXYZ > 147.970147){
                                    state=WALKING;
                                }
                            }
                        }
                    }
                    if(SumDifPre > 11.475189){
                        if(SumDifPre <= 16.669757){
                            state=WALKING;
                        }
                        if(SumDifPre > 16.669757){
                            if(SumSqBpfGyrXYZ <= 88.094128){
                                if(SumDifPre <= 47.022883){
                                    if(SumSqLpfDifAccXYZ <= 1176.189771){
                                        state=WALKING;
                                    }
                                    if(SumSqLpfDifAccXYZ > 1176.189771){
                                        state=WALKUP;
                                    }
                                }
                                if(SumDifPre > 47.022883){
                                    state=STATIONARY;
                                }
                            }
                            if(SumSqBpfGyrXYZ > 88.094128){
                                state=WALKUP;
                            }
                        }
                    }
                }
                if(SumSqLpfDifAccXYZ > 2344.958328){
                    if(SumDifPre <= 7.764658){
                        if(SumSqLpfDifAccXYZ <= 9100.784382){
                            if(SumDifPre <= -7.828931){
                                if(SumSqBpfGyrXYZ <= 108.139313){
                                    state=TRANSITION;
                                }
                                if(SumSqBpfGyrXYZ > 108.139313){
                                    if(SumSqBpfGyrXYZ <= 137.232379){
                                        if(SumDifPre <= -14.039658){
                                            state=WALKDN;
                                        }
                                        if(SumDifPre > -14.039658){
                                            state=WALKING;
                                        }
                                    }
                                    if(SumSqBpfGyrXYZ > 137.232379){
                                        state=WALKING;
                                    }
                                }
                            }
                            if(SumDifPre > -7.828931){
                                if(SumSqBpfGyrXYZ <= 59.87081){
                                    if(SumSqLpfDifAccXYZ <= 3174.645068){
                                        if(SumDifPre <= 1.849623){
                                            state=WALKING;
                                        }
                                        if(SumDifPre > 1.849623){
                                            state=TRANSITION;
                                        }
                                    }
                                    if(SumSqLpfDifAccXYZ > 3174.645068){
                                        state=TRANSITION;
                                    }
                                }
                                if(SumSqBpfGyrXYZ > 59.87081){
                                    if(SumSqLpfDifAccXYZ <= 4321.173188){
                                        if(SumSqBpfGyrXYZ <= 182.883853){
                                            if(SumDifPre <= 2.985146){
                                                if(SumSqLpfDifAccXYZ <= 3736.751181){
                                                    state=WALKING;
                                                }
                                                if(SumSqLpfDifAccXYZ > 3736.751181){
                                                    state=TRANSITION;
                                                }
                                            }
                                            if(SumDifPre > 2.985146){
                                                state=WALKING;
                                            }
                                        }
                                        if(SumSqBpfGyrXYZ > 182.883853){
                                            state=STATIONARY;
                                        }
                                    }
                                    if(SumSqLpfDifAccXYZ > 4321.173188){
                                        if(SumSqBpfGyrXYZ <= 258.177081){
                                            state=TRANSITION;
                                        }
                                        if(SumSqBpfGyrXYZ > 258.177081){
                                            state=WALKING;
                                        }
                                    }
                                }
                            }
                        }
                        if(SumSqLpfDifAccXYZ > 9100.784382){
                            state=TRANSITION;
                        }
                    }
                    if(SumDifPre > 7.764658){
                        if(SumSqLpfDifAccXYZ <= 7289.835981){
                            if(SumDifPre <= 22.055433){
                                if(SumSqBpfGyrXYZ <= 58.554248){
                                    if(SumSqBpfGyrXYZ <= 50.131767){
                                        if(SumDifPre <= 17.732224){
                                            if(SumSqBpfGyrXYZ <= 40.258292){
                                                if(SumSqBpfGyrXYZ <= 37.780547){
                                                    if(SumSqBpfGyrXYZ <= 33.041815){
                                                        state=WALKUP;
                                                    }
                                                    if(SumSqBpfGyrXYZ > 33.041815){
                                                        if(SumSqBpfGyrXYZ <= 35.833275){
                                                            if(SumSqLpfDifAccXYZ <= 3888.201646){
                                                                state=STATIONARY;
                                                            }
                                                            if(SumSqLpfDifAccXYZ > 3888.201646){
                                                                state=TRANSITION;
                                                            }
                                                        }
                                                        if(SumSqBpfGyrXYZ > 35.833275){
                                                            state=TRANSITION;
                                                        }
                                                    }
                                                }
                                                if(SumSqBpfGyrXYZ > 37.780547){
                                                    state=WALKUP;
                                                }
                                            }
                                            if(SumSqBpfGyrXYZ > 40.258292){
                                                if(SumSqLpfDifAccXYZ <= 4337.956717){
                                                    state=STATIONARY;
                                                }
                                                if(SumSqLpfDifAccXYZ > 4337.956717){
                                                    state=TRANSITION;
                                                }
                                            }
                                        }
                                        if(SumDifPre > 17.732224){
                                            state=WALKUP;
                                        }
                                    }
                                    if(SumSqBpfGyrXYZ > 50.131767){
                                        state=WALKUP;
                                    }
                                }
                                if(SumSqBpfGyrXYZ > 58.554248){
                                    state=WALKUP;
                                }
                            }
                            if(SumDifPre > 22.055433){
                                state=WALKUP;
                            }
                        }
                        if(SumSqLpfDifAccXYZ > 7289.835981){
                            state=TRANSITION;
                        }
                    }
                }
            }
        }
        return state;
    }


    public int classifyFiveFeatures(double SumGloAccXY, double SumGloGyrXY, double SumDifPre, double SumDifIncAng, double SumDifAccZ){
        state = -1;
        if(SumGloGyrXY <= 23.966792){
            if(SumGloGyrXY <= 3.371761){
                state=STATIONARY;
            }
            if(SumGloGyrXY > 3.371761){
                if(SumDifIncAng <= 93.433831){
                    if(SumGloAccXY <= 57.984053){
                        if(SumDifIncAng <= 80.155056){
                            state=TRANSITION;
                        }
                        if(SumDifIncAng > 80.155056){
                            state=STATIONARY;
                        }
                    }
                    if(SumGloAccXY > 57.984053){
                        if(SumDifAccZ <= 125.150662){
                            if(SumGloGyrXY <= 20.538278){
                                if(SumDifPre <= -10.15634){
                                    state=STATIONARY;
                                }
                                if(SumDifPre > -10.15634){
                                    if(SumDifAccZ <= 18.044813){
                                        if(SumDifIncAng <= 69.646994){
                                            if(SumGloAccXY <= 7149.723986){
                                                if(SumGloGyrXY <= 17.306843){
                                                    state=WALKING;
                                                }
                                                if(SumGloGyrXY > 17.306843){
                                                    state=STATIONARY;
                                                }
                                            }
                                            if(SumGloAccXY > 7149.723986){
                                                state=STATIONARY;
                                            }
                                        }
                                        if(SumDifIncAng > 69.646994){
                                            state=STATIONARY;
                                        }
                                    }
                                    if(SumDifAccZ > 18.044813){
                                        state=STATIONARY;
                                    }
                                }
                            }
                            if(SumGloGyrXY > 20.538278){
                                state=STATIONARY;
                            }
                        }
                        if(SumDifAccZ > 125.150662){
                            if(SumDifAccZ <= 504.338588){
                                if(SumGloAccXY <= 431.46292){
                                    state=WALKING;
                                }
                                if(SumGloAccXY > 431.46292){
                                    if(SumDifPre <= -14.301981){
                                        state=TRANSITION;
                                    }
                                    if(SumDifPre > -14.301981){
                                        if(SumGloGyrXY <= 13.53773){
                                            state=STATIONARY;
                                        }
                                        if(SumGloGyrXY > 13.53773){
                                            if(SumDifIncAng <= 49.454685){
                                                state=WALKING;
                                            }
                                            if(SumDifIncAng > 49.454685){
                                                if(SumGloAccXY <= 4105.839533){
                                                    if(SumDifAccZ <= 240.894499){
                                                        state=STATIONARY;
                                                    }
                                                    if(SumDifAccZ > 240.894499){
                                                        state=WALKING;
                                                    }
                                                }
                                                if(SumGloAccXY > 4105.839533){
                                                    state=STATIONARY;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if(SumDifAccZ > 504.338588){
                                state=WALKUP;
                            }
                        }
                    }
                }
                if(SumDifIncAng > 93.433831){
                    if(SumGloGyrXY <= 11.278178){
                        state=STATIONARY;
                    }
                    if(SumGloGyrXY > 11.278178){
                        if(SumGloAccXY <= 70.477175){
                            state=STATIONARY;
                        }
                        if(SumGloAccXY > 70.477175){
                            if(SumDifPre <= -8.847475){
                                state=STATIONARY;
                            }
                            if(SumDifPre > -8.847475){
                                if(SumGloGyrXY <= 14.863534){
                                    state=STATIONARY;
                                }
                                if(SumGloGyrXY > 14.863534){
                                    if(SumGloAccXY <= 160.009396){
                                        if(SumDifPre <= -0.499653){
                                            if(SumDifAccZ <= 12.092346){
                                                state=STATIONARY;
                                            }
                                            if(SumDifAccZ > 12.092346){
                                                state=TRANSITION;
                                            }
                                        }
                                        if(SumDifPre > -0.499653){
                                            state=STATIONARY;
                                        }
                                    }
                                    if(SumGloAccXY > 160.009396){
                                        if(SumDifIncAng <= 99.301874){
                                            state=STATIONARY;
                                        }
                                        if(SumDifIncAng > 99.301874){
                                            state=WALKING;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if(SumGloGyrXY > 23.966792){
            if(SumDifIncAng <= 89.086535){
                if(SumGloAccXY <= 438.636363){
                    if(SumDifIncAng <= 73.880665){
                        if(SumDifIncAng <= 46.984921){
                            state=TRANSITION;
                        }
                        if(SumDifIncAng > 46.984921){
                            if(SumDifPre <= 12.543664){
                                if(SumGloAccXY <= 160.22755){
                                    if(SumGloGyrXY <= 65.888515){
                                        state=TRANSITION;
                                    }
                                    if(SumGloGyrXY > 65.888515){
                                        if(SumDifPre <= 10.714679){
                                            state=TRANSITION;
                                        }
                                        if(SumDifPre > 10.714679){
                                            state=STATIONARY;
                                        }
                                    }
                                }
                                if(SumGloAccXY > 160.22755){
                                    if(SumDifAccZ <= 77.694241){
                                        if(SumDifPre <= 6.916411){
                                            if(SumDifAccZ <= 16.33646){
                                                state=TRANSITION;
                                            }
                                            if(SumDifAccZ > 16.33646){
                                                if(SumDifIncAng <= 68.998908){
                                                    state=TRANSITION;
                                                }
                                                if(SumDifIncAng > 68.998908){
                                                    state=STATIONARY;
                                                }
                                            }
                                        }
                                        if(SumDifPre > 6.916411){
                                            state=TRANSITION;
                                        }
                                    }
                                    if(SumDifAccZ > 77.694241){
                                        if(SumDifPre <= 5.390014){
                                            state=WALKING;
                                        }
                                        if(SumDifPre > 5.390014){
                                            state=STATIONARY;
                                        }
                                    }
                                }
                            }
                            if(SumDifPre > 12.543664){
                                if(SumGloGyrXY <= 96.318717){
                                    state=TRANSITION;
                                }
                                if(SumGloGyrXY > 96.318717){
                                    state=WALKUP;
                                }
                            }
                        }
                    }
                    if(SumDifIncAng > 73.880665){
                        if(SumDifPre <= 10.91449){
                            if(SumGloAccXY <= 229.099618){
                                if(SumDifPre <= 4.94065){
                                    state=STATIONARY;
                                }
                                if(SumDifPre > 4.94065){
                                    if(SumGloGyrXY <= 44.500998){
                                        if(SumDifIncAng <= 83.615981){
                                            state=TRANSITION;
                                        }
                                        if(SumDifIncAng > 83.615981){
                                            state=STATIONARY;
                                        }
                                    }
                                    if(SumGloGyrXY > 44.500998){
                                        state=TRANSITION;
                                    }
                                }
                            }
                            if(SumGloAccXY > 229.099618){
                                state=WALKING;
                            }
                        }
                        if(SumDifPre > 10.91449){
                            if(SumGloGyrXY <= 66.355329){
                                if(SumGloAccXY <= 167.225877){
                                    if(SumGloGyrXY <= 38.330433){
                                        state=TRANSITION;
                                    }
                                    if(SumGloGyrXY > 38.330433){
                                        if(SumDifAccZ <= -10.425949){
                                            state=STATIONARY;
                                        }
                                        if(SumDifAccZ > -10.425949){
                                            state=WALKUP;
                                        }
                                    }
                                }
                                if(SumGloAccXY > 167.225877){
                                    if(SumDifPre <= 22.696635){
                                        if(SumGloGyrXY <= 37.758579){
                                            state=STATIONARY;
                                        }
                                        if(SumGloGyrXY > 37.758579){
                                            state=TRANSITION;
                                        }
                                    }
                                    if(SumDifPre > 22.696635){
                                        state=WALKING;
                                    }
                                }
                            }
                            if(SumGloGyrXY > 66.355329){
                                state=WALKUP;
                            }
                        }
                    }
                }
                if(SumGloAccXY > 438.636363){
                    if(SumDifPre <= 13.477925){
                        if(SumDifIncAng <= 69.124787){
                            if(SumDifAccZ <= 57.359794){
                                if(SumDifIncAng <= 59.419913){
                                    if(SumGloAccXY <= 5295.884019){
                                        if(SumDifPre <= -0.7629){
                                            if(SumDifPre <= -14.951247){
                                                state=TRANSITION;
                                            }
                                            if(SumDifPre > -14.951247){
                                                if(SumDifAccZ <= 20.325077){
                                                    if(SumDifIncAng <= 33.809677){
                                                        state=TRANSITION;
                                                    }
                                                    if(SumDifIncAng > 33.809677){
                                                        if(SumDifAccZ <= -263.091161){
                                                            state=STATIONARY;
                                                        }
                                                        if(SumDifAccZ > -263.091161){
                                                            state=TRANSITION;
                                                        }
                                                    }
                                                }
                                                if(SumDifAccZ > 20.325077){
                                                    state=TRANSITION;
                                                }
                                            }
                                        }
                                        if(SumDifPre > -0.7629){
                                            state=TRANSITION;
                                        }
                                    }
                                    if(SumGloAccXY > 5295.884019){
                                        if(SumGloGyrXY <= 65.008558){
                                            state=STATIONARY;
                                        }
                                        if(SumGloGyrXY > 65.008558){
                                            if(SumDifIncAng <= 32.086114){
                                                if(SumDifIncAng <= -3.563058){
                                                    state=TRANSITION;
                                                }
                                                if(SumDifIncAng > -3.563058){
                                                    state=WALKING;
                                                }
                                            }
                                            if(SumDifIncAng > 32.086114){
                                                state=WALKING;
                                            }
                                        }
                                    }
                                }
                                if(SumDifIncAng > 59.419913){
                                    if(SumGloGyrXY <= 36.982992){
                                        state=STATIONARY;
                                    }
                                    if(SumGloGyrXY > 36.982992){
                                        state=WALKING;
                                    }
                                }
                            }
                            if(SumDifAccZ > 57.359794){
                                if(SumDifPre <= -9.105467){
                                    state=TRANSITION;
                                }
                                if(SumDifPre > -9.105467){
                                    if(SumGloGyrXY <= 63.93266){
                                        if(SumDifPre <= -0.85569){
                                            state=STATIONARY;
                                        }
                                        if(SumDifPre > -0.85569){
                                            if(SumGloAccXY <= 6151.62781){
                                                if(SumDifIncAng <= 36.244112){
                                                    state=WALKING;
                                                }
                                                if(SumDifIncAng > 36.244112){
                                                    if(SumGloAccXY <= 1350.351738){
                                                        if(SumGloGyrXY <= 56.750444){
                                                            if(SumGloAccXY <= 757.160664){
                                                                if(SumGloGyrXY <= 34.874671){
                                                                    state=WALKING;
                                                                }
                                                                if(SumGloGyrXY > 34.874671){
                                                                    if(SumDifPre <= 3.385813){
                                                                        state=STATIONARY;
                                                                    }
                                                                    if(SumDifPre > 3.385813){
                                                                        state=WALKING;
                                                                    }
                                                                }
                                                            }
                                                            if(SumGloAccXY > 757.160664){
                                                                state=WALKING;
                                                            }
                                                        }
                                                        if(SumGloGyrXY > 56.750444){
                                                            state=STATIONARY;
                                                        }
                                                    }
                                                    if(SumGloAccXY > 1350.351738){
                                                        if(SumDifPre <= 6.571181){
                                                            if(SumGloGyrXY <= 42.228092){
                                                                state=STATIONARY;
                                                            }
                                                            if(SumGloGyrXY > 42.228092){
                                                                if(SumDifPre <= -0.395917){
                                                                    state=WALKING;
                                                                }
                                                                if(SumDifPre > -0.395917){
                                                                    if(SumGloGyrXY <= 61.460492){
                                                                        state=STATIONARY;
                                                                    }
                                                                    if(SumGloGyrXY > 61.460492){
                                                                        state=WALKING;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if(SumDifPre > 6.571181){
                                                            state=WALKING;
                                                        }
                                                    }
                                                }
                                            }
                                            if(SumGloAccXY > 6151.62781){
                                                state=STATIONARY;
                                            }
                                        }
                                    }
                                    if(SumGloGyrXY > 63.93266){
                                        if(SumDifIncAng <= -7.312899){
                                            if(SumDifIncAng <= -33.143841){
                                                state=STATIONARY;
                                            }
                                            if(SumDifIncAng > -33.143841){
                                                if(SumDifPre <= 6.212969){
                                                    if(SumDifIncAng <= -11.41152){
                                                        state=WALKING;
                                                    }
                                                    if(SumDifIncAng > -11.41152){
                                                        state=STATIONARY;
                                                    }
                                                }
                                                if(SumDifPre > 6.212969){
                                                    state=STATIONARY;
                                                }
                                            }
                                        }
                                        if(SumDifIncAng > -7.312899){
                                            state=WALKING;
                                        }
                                    }
                                }
                            }
                        }
                        if(SumDifIncAng > 69.124787){
                            if(SumGloGyrXY <= 51.632201){
                                state=STATIONARY;
                            }
                            if(SumGloGyrXY > 51.632201){
                                if(SumDifPre <= -14.834106){
                                    if(SumGloAccXY <= 1038.768481){
                                        state=TRANSITION;
                                    }
                                    if(SumGloAccXY > 1038.768481){
                                        if(SumGloGyrXY <= 104.379905){
                                            if(SumGloGyrXY <= 82.546303){
                                                state=WALKING;
                                            }
                                            if(SumGloGyrXY > 82.546303){
                                                if(SumGloGyrXY <= 97.331933){
                                                    state=TRANSITION;
                                                }
                                                if(SumGloGyrXY > 97.331933){
                                                    state=WALKING;
                                                }
                                            }
                                        }
                                        if(SumGloGyrXY > 104.379905){
                                            state=WALKING;
                                        }
                                    }
                                }
                                if(SumDifPre > -14.834106){
                                    state=WALKING;
                                }
                            }
                        }
                    }
                    if(SumDifPre > 13.477925){
                        if(SumDifIncAng <= 58.131045){
                            if(SumDifPre <= 36.13338){
                                if(SumGloAccXY <= 634.985068){
                                    if(SumDifAccZ <= 40.477718){
                                        state=TRANSITION;
                                    }
                                    if(SumDifAccZ > 40.477718){
                                        state=WALKING;
                                    }
                                }
                                if(SumGloAccXY > 634.985068){
                                    if(SumGloGyrXY <= 135.202215){
                                        if(SumDifAccZ <= 15.909922){
                                            state=TRANSITION;
                                        }
                                        if(SumDifAccZ > 15.909922){
                                            state=WALKING;
                                        }
                                    }
                                    if(SumGloGyrXY > 135.202215){
                                        state=WALKING;
                                    }
                                }
                            }
                            if(SumDifPre > 36.13338){
                                state=WALKUP;
                            }
                        }
                        if(SumDifIncAng > 58.131045){
                            if(SumGloGyrXY <= 125.331986){
                                if(SumGloGyrXY <= 55.31039){
                                    if(SumDifIncAng <= 81.229642){
                                        state=STATIONARY;
                                    }
                                    if(SumDifIncAng > 81.229642){
                                        state=WALKING;
                                    }
                                }
                                if(SumGloGyrXY > 55.31039){
                                    state=WALKING;
                                }
                            }
                            if(SumGloGyrXY > 125.331986){
                                state=WALKUP;
                            }
                        }
                    }
                }
            }
            if(SumDifIncAng > 89.086535){
                if(SumDifPre <= -15.135134){
                    if(SumDifPre <= -23.855606){
                        if(SumDifPre <= -60.788336){
                            state=STATIONARY;
                        }
                        if(SumDifPre > -60.788336){
                            state=WALKDN;
                        }
                    }
                    if(SumDifPre > -23.855606){
                        if(SumDifAccZ <= -4.64123){
                            if(SumDifIncAng <= 99.618175){
                                if(SumDifIncAng <= 94.999294){
                                    if(SumGloGyrXY <= 115.911466){
                                        state=WALKING;
                                    }
                                    if(SumGloGyrXY > 115.911466){
                                        state=WALKDN;
                                    }
                                }
                                if(SumDifIncAng > 94.999294){
                                    state=WALKDN;
                                }
                            }
                            if(SumDifIncAng > 99.618175){
                                if(SumGloAccXY <= 910.442387){
                                    state=WALKDN;
                                }
                                if(SumGloAccXY > 910.442387){
                                    state=WALKING;
                                }
                            }
                        }
                        if(SumDifAccZ > -4.64123){
                            if(SumGloGyrXY <= 50.141266){
                                if(SumGloAccXY <= 174.93966){
                                    state=STATIONARY;
                                }
                                if(SumGloAccXY > 174.93966){
                                    if(SumDifIncAng <= 95.981258){
                                        state=WALKING;
                                    }
                                    if(SumDifIncAng > 95.981258){
                                        if(SumDifIncAng <= 99.464971){
                                            if(SumDifPre <= -19.020379){
                                                state=WALKDN;
                                            }
                                            if(SumDifPre > -19.020379){
                                                state=WALKING;
                                            }
                                        }
                                        if(SumDifIncAng > 99.464971){
                                            state=WALKING;
                                        }
                                    }
                                }
                            }
                            if(SumGloGyrXY > 50.141266){
                                if(SumDifIncAng <= 98.813031){
                                    state=WALKING;
                                }
                                if(SumDifIncAng > 98.813031){
                                    if(SumGloAccXY <= 967.664823){
                                        if(SumDifAccZ <= 5.947216){
                                            if(SumGloGyrXY <= 144.594044){
                                                state=WALKING;
                                            }
                                            if(SumGloGyrXY > 144.594044){
                                                state=WALKDN;
                                            }
                                        }
                                        if(SumDifAccZ > 5.947216){
                                            state=WALKDN;
                                        }
                                    }
                                    if(SumGloAccXY > 967.664823){
                                        state=WALKING;
                                    }
                                }
                            }
                        }
                    }
                }
                if(SumDifPre > -15.135134){
                    if(SumDifPre <= 14.116604){
                        if(SumGloAccXY <= 365.85653){
                            if(SumGloAccXY <= 174.125573){
                                state=STATIONARY;
                            }
                            if(SumGloAccXY > 174.125573){
                                if(SumDifIncAng <= 98.831699){
                                    if(SumDifPre <= -9.724166){
                                        state=WALKING;
                                    }
                                    if(SumDifPre > -9.724166){
                                        if(SumGloGyrXY <= 35.685972){
                                            state=WALKING;
                                        }
                                        if(SumGloGyrXY > 35.685972){
                                            if(SumGloGyrXY <= 99.480014){
                                                state=WALKING;
                                            }
                                            if(SumGloGyrXY > 99.480014){
                                                if(SumDifIncAng <= 91.838801){
                                                    state=STATIONARY;
                                                }
                                                if(SumDifIncAng > 91.838801){
                                                    state=WALKING;
                                                }
                                            }
                                        }
                                    }
                                }
                                if(SumDifIncAng > 98.831699){
                                    state=WALKING;
                                }
                            }
                        }
                        if(SumGloAccXY > 365.85653){
                            if(SumGloGyrXY <= 83.458409){
                                if(SumDifIncAng <= 99.04533){
                                    if(SumDifPre <= -6.951298){
                                        if(SumDifAccZ <= 6.414544){
                                            if(SumGloGyrXY <= 56.705674){
                                                if(SumDifAccZ <= -0.513808){
                                                    state=WALKING;
                                                }
                                                if(SumDifAccZ > -0.513808){
                                                    if(SumDifIncAng <= 98.581405){
                                                        if(SumDifPre <= -7.545471){
                                                            if(SumGloAccXY <= 508.236513){
                                                                if(SumDifPre <= -11.701787){
                                                                    state=STATIONARY;
                                                                }
                                                                if(SumDifPre > -11.701787){
                                                                    state=WALKING;
                                                                }
                                                            }
                                                            if(SumGloAccXY > 508.236513){
                                                                state=WALKING;
                                                            }
                                                        }
                                                        if(SumDifPre > -7.545471){
                                                            state=STATIONARY;
                                                        }
                                                    }
                                                    if(SumDifIncAng > 98.581405){
                                                        state=STATIONARY;
                                                    }
                                                }
                                            }
                                            if(SumGloGyrXY > 56.705674){
                                                state=WALKING;
                                            }
                                        }
                                        if(SumDifAccZ > 6.414544){
                                            state=WALKING;
                                        }
                                    }
                                    if(SumDifPre > -6.951298){
                                        state=WALKING;
                                    }
                                }
                                if(SumDifIncAng > 99.04533){
                                    state=WALKING;
                                }
                            }
                            if(SumGloGyrXY > 83.458409){
                                state=WALKING;
                            }
                        }
                    }
                    if(SumDifPre > 14.116604){
                        if(SumDifPre <= 22.206454){
                            if(SumDifAccZ <= 4.547273){
                                if(SumGloGyrXY <= 31.210398){
                                    if(SumDifIncAng <= 93.302154){
                                        state=TRANSITION;
                                    }
                                    if(SumDifIncAng > 93.302154){
                                        if(SumGloAccXY <= 191.176078){
                                            state=STATIONARY;
                                        }
                                        if(SumGloAccXY > 191.176078){
                                            state=WALKING;
                                        }
                                    }
                                }
                                if(SumGloGyrXY > 31.210398){
                                    if(SumDifIncAng <= 97.748955){
                                        if(SumGloGyrXY <= 75.656151){
                                            state=WALKING;
                                        }
                                        if(SumGloGyrXY > 75.656151){
                                            if(SumGloAccXY <= 710.065108){
                                                state=WALKUP;
                                            }
                                            if(SumGloAccXY > 710.065108){
                                                state=WALKING;
                                            }
                                        }
                                    }
                                    if(SumDifIncAng > 97.748955){
                                        state=WALKING;
                                    }
                                }
                            }
                            if(SumDifAccZ > 4.547273){
                                if(SumGloGyrXY <= 41.928984){
                                    state=STATIONARY;
                                }
                                if(SumGloGyrXY > 41.928984){
                                    if(SumGloAccXY <= 1445.092892){
                                        if(SumGloGyrXY <= 133.111321){
                                            if(SumGloAccXY <= 388.532024){
                                                state=WALKUP;
                                            }
                                            if(SumGloAccXY > 388.532024){
                                                if(SumGloGyrXY <= 89.049751){
                                                    state=WALKING;
                                                }
                                                if(SumGloGyrXY > 89.049751){
                                                    if(SumDifPre <= 14.908743){
                                                        state=WALKING;
                                                    }
                                                    if(SumDifPre > 14.908743){
                                                        state=WALKUP;
                                                    }
                                                }
                                            }
                                        }
                                        if(SumGloGyrXY > 133.111321){
                                            state=WALKUP;
                                        }
                                    }
                                    if(SumGloAccXY > 1445.092892){
                                        state=WALKING;
                                    }
                                }
                            }
                        }
                        if(SumDifPre > 22.206454){
                            if(SumGloGyrXY <= 85.373798){
                                if(SumGloGyrXY <= 35.162921){
                                    if(SumDifPre <= 27.201233){
                                        state=WALKING;
                                    }
                                    if(SumDifPre > 27.201233){
                                        state=STATIONARY;
                                    }
                                }
                                if(SumGloGyrXY > 35.162921){
                                    if(SumGloGyrXY <= 66.492891){
                                        state=WALKING;
                                    }
                                    if(SumGloGyrXY > 66.492891){
                                        if(SumGloAccXY <= 538.424032){
                                            state=WALKUP;
                                        }
                                        if(SumGloAccXY > 538.424032){
                                            state=WALKING;
                                        }
                                    }
                                }
                            }
                            if(SumGloGyrXY > 85.373798){
                                if(SumDifIncAng <= 99.385746){
                                    if(SumGloAccXY <= 571.309799){
                                        state=WALKUP;
                                    }
                                    if(SumGloAccXY > 571.309799){
                                        if(SumGloGyrXY <= 171.074529){
                                            if(SumGloAccXY <= 1232.491624){
                                                state=WALKUP;
                                            }
                                            if(SumGloAccXY > 1232.491624){
                                                state=WALKING;
                                            }
                                        }
                                        if(SumGloGyrXY > 171.074529){
                                            if(SumGloAccXY <= 2425.949455){
                                                state=WALKUP;
                                            }
                                            if(SumGloAccXY > 2425.949455){
                                                state=WALKING;
                                            }
                                        }
                                    }
                                }
                                if(SumDifIncAng > 99.385746){
                                    if(SumGloAccXY <= 1070.557592){
                                        state=WALKUP;
                                    }
                                    if(SumGloAccXY > 1070.557592){
                                        state=WALKING;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return state;
    }
}
