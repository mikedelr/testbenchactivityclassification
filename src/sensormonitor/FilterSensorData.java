package sensormonitor;

import android.ContentValues;
import android.Context;
import android.Log;

import java.util.ArrayList;

/**
 * Created by Michael Del Rosario on 14/01/2016.
 */
public class FilterSensorData {
    public static final String TAG = "FilterSensorData";
    protected static final int TWO_AND_A_HALF_SEC = 100; //2.50 seconds @ 40Hz
    protected static final int ONE_AND_A_QTR_SEC  = 50;  //1.25 seconds @ 40Hz
    protected ContentValues _activityContentValues = new ContentValues();
    public static final int ACC_GYR_MAG_DELAY_MICROS = 25000; // 40hz
    public static final int PRESSURE_DELAY_MICROS    = 50000; // 20Hz
    public static final double GYR_DT = (double)ACC_GYR_MAG_DELAY_MICROS/1000000;
    protected static int RUNNING_BUFFER_SIZE = 128;
    protected static int FILTER_BUFFER_SIZE = 512;
    protected static int WINDOW_BUFFER_SIZE = 256;
    protected static int MAX_BUFFER_SIZE = Math.max(Math.max(RUNNING_BUFFER_SIZE,FILTER_BUFFER_SIZE),WINDOW_BUFFER_SIZE);
    protected static int CNT_RESET_VALUE = MAX_BUFFER_SIZE*MAX_BUFFER_SIZE*MAX_BUFFER_SIZE;
    protected final int CONV_WIN_SECS = 5*40; /** 5 seconds times the sampling rate **/ //TODO: Update this value
    protected boolean bConv = false;
    protected boolean bDifPreConv = false;
    protected Context _context;


    protected static int DB_INSERT_CTR_LIMIT = 60;
    protected ArrayList<Integer> activityTypeList = new ArrayList<Integer>();
    protected ArrayList<Long>    activityTimeList = new ArrayList<Long>();
//    protected ArrayList<ContentProviderOperation> insertOperations = new ArrayList<ContentProviderOperation>();
    protected int dbInsertCtr = 0;

    protected ContentValues _contentValuesArrayActivities[];// = new ContentValues[DB_INSERT_CTR_LIMIT];

    protected int smpCtr = 0;
    protected int barSmpCtr = 0;

    /** FilterHandler Objects **/
    protected FilterHandler filtHandLpfDiffBar = new FilterHandler();

    protected FilterHandler filtHandLpfDifAccX = new FilterHandler();
    protected FilterHandler filtHandLpfDifAccY = new FilterHandler();
    protected FilterHandler filtHandLpfDifAccZ = new FilterHandler();

    protected FilterHandler filtHandBpfGyrX = new FilterHandler();
    protected FilterHandler filtHandBpfGyrY = new FilterHandler();
    protected FilterHandler filtHandBpfGyrZ = new FilterHandler();

    /** Buffers for DSP on raw signals **/
    protected double [] ACCX_BUFFER = new double[FILTER_BUFFER_SIZE];
    protected double [] ACCY_BUFFER = new double[FILTER_BUFFER_SIZE];
    protected double [] ACCZ_BUFFER = new double[FILTER_BUFFER_SIZE];
    protected double [] GYRX_BUFFER = new double[FILTER_BUFFER_SIZE];
    protected double [] GYRY_BUFFER = new double[FILTER_BUFFER_SIZE];
    protected double [] GYRZ_BUFFER = new double[FILTER_BUFFER_SIZE];
    protected double [] BAR_BUFFER  = new double[FILTER_BUFFER_SIZE];

    /** Store the most recent 'filtered' values**/
    protected double difPre       = 0;

    protected double lpfDifAccX   = 0;
    protected double lpfDifAccY   = 0;
    protected double lpfDifAccZ   = 0;
    protected double sumSqLpfDifAccXYZ = 0;

    protected double bpfGyrX = 0;
    protected double bpfGyrY = 0;
    protected double bpfGyrZ = 0;
    protected double sumSqBpfGyrXYZ = 0;

    /** Windows used to calculate a running sum **/
    protected double [] DIF_PRE_RUN_SUM         = new double[RUNNING_BUFFER_SIZE];
    protected double [] LPF_DIF_ACC_XYZ_RUN_SUM = new double[RUNNING_BUFFER_SIZE];
    protected double [] BPF_GYR_XYZ_RUN_SUM     = new double[RUNNING_BUFFER_SIZE];

    /** Window Handlers for calculating the running sum **/
    WindowHandler difPreWinHand    = new WindowHandler();

    WindowHandler lpfDifAccWinHandXYZ = new WindowHandler();
    WindowHandler bpfGyrWinHandXYZ = new WindowHandler();

    /** Running sum Values for accumulated features **/
    protected double runSumDifPre     = 0;

    protected double runSumSqBpfGyrXYZ = 0;
    protected double runSumSqLpfDifAccXYZ = 0;

    /** Store running sum values for **/
    protected double[] DIF_PRE_WIN       = new double[WINDOW_BUFFER_SIZE];
    protected double[] SUM_SQ_LPF_DIF_ACC_XYZ_WIN = new double[WINDOW_BUFFER_SIZE];
    protected double[] SUM_SQ_BPF_GYR_XYZ_WIN = new double[WINDOW_BUFFER_SIZE];

    /** Synchronised Features extracted**/
    protected double synSumDifPre       = 0;
    protected double synSumSqGloAccXY   = 0;
    protected double synSumSqGloGyrXY   = 0;
    protected double synDifIncAng       = 0;
    protected double synSumLpfDifAccZ   = 0;
    protected double synSumSqLpfDifAccZ = 0;

    /** Counter for determining when to classify the activity**/
    protected int featureCtr           = 0;
    protected boolean bClassifyNow     = false;
    public static int TIME_TO_CLASSIFY = ONE_AND_A_QTR_SEC;

    /** Feature Buffer Handlers **/
    protected FeatureHandler difPreFeatHand       = new FeatureHandler();
    protected FeatureHandler lpfDifAccXYZFeatHand = new FeatureHandler();
    protected FeatureHandler bpfGyrXYZFeatHand    = new FeatureHandler();

    protected FeatureHandler gloAccXYFeatHand     = new FeatureHandler();
    protected FeatureHandler gloGyrXYFeatHand     = new FeatureHandler();
    protected FeatureHandler difIncAngFeatHand    = new FeatureHandler();
    protected FeatureHandler lpfDifAccZFeatHand   = new FeatureHandler();
    protected FeatureHandler sqLpfDifAccZFeatHand = new FeatureHandler();

    /** Feature vectors for classification**/
    protected double featSumDifPre       = 0;
    protected double featSumSqLpfDifAccXYZ = 0;
    protected double featSumSqBpfGyrXYZ    = 0;

    protected double featSumSqGloAccXY   = 0;
    protected double featSumSqGloGyrXY   = 0;
    protected double featSumDifIncAng    = 0;
    protected double featSumLpfDifAccZ   = 0;
    protected double featSumSqLpfDifAccZ = 0;

    protected ClassifierBasic classifierBasic = new ClassifierBasic();

    protected int stateIdentified = -1;
    protected long _activityTime;

    public FilterSensorData(){

    }
    /**
     * Default constructor
     */
    public FilterSensorData(Context context){
        this._context = context;
    }

    public void close(){
//        //TODO:: Check ContentValues[] object is empty, if not write to buffer
//        if (insertOperations.size() > 0) {
//            Log.d(TAG,"inserting before close");
//            try {
//                _context.getContentResolver().applyBatch(PatientContentProvider.AUTHORITY, insertOperations);
//            } catch (RemoteException e) {
//                e.printStackTrace();
//            } catch (OperationApplicationException e) {
//                e.printStackTrace();
//            }
//            insertOperations.clear();
//        }
    }

    /** Reset the sample counters to zero once they reach a pre-defined maximum value**/
    public void checkCounters(){
        if(smpCtr == CNT_RESET_VALUE){
            smpCtr = 0;
        }
        if(barSmpCtr == CNT_RESET_VALUE){
            barSmpCtr = 0;
        }
    }

    /**
     * This method encapsulates all of the signal processing, feature extraction methods necessary
     * to generate the feature vector which will be used by a decision tree algorithm to perform the activity
     * classification algorithm
     * @param sensorData
     */
    public void update(SensorData sensorData){
        stateIdentified = -1;
        checkCounters();
//        Log.d(TAG, "smpCtr:" + smpCtr);
        /** Process barometer **/
        if(sensorData.barLoaded){
//            Log.d(TAG, "smpCtr: " + smpCtr + " barSmpCtr: " + barSmpCtr);
            filtHandLpfDiffBar.fillBuffer(barSmpCtr, sensorData.bar, BAR_BUFFER);
            if (!bDifPreConv) {
                if (!(barSmpCtr <FilterCoefficients.difPre80.length)) {
                    bDifPreConv = true;
                }
            }
            if (bDifPreConv) {
                difPre = filtHandLpfDiffBar.filterSignal(barSmpCtr, BAR_BUFFER, FilterCoefficients.difPre80);
            }
            barSmpCtr++;
        }
        /** Process Accelerometer Signals**/
        filtHandLpfDifAccX.fillBuffer(smpCtr, sensorData.acc[0], ACCX_BUFFER);
        filtHandLpfDifAccY.fillBuffer(smpCtr, sensorData.acc[1], ACCY_BUFFER);
        filtHandLpfDifAccZ.fillBuffer(smpCtr, sensorData.acc[2], ACCZ_BUFFER);

        lpfDifAccX   = filtHandLpfDifAccX.filterSignal(smpCtr, ACCX_BUFFER, FilterCoefficients.lpfDif50);
        lpfDifAccY   = filtHandLpfDifAccY.filterSignal(smpCtr, ACCY_BUFFER, FilterCoefficients.lpfDif50);
        lpfDifAccZ   = filtHandLpfDifAccZ.filterSignal(smpCtr, ACCZ_BUFFER, FilterCoefficients.lpfDif50);

        sumSqLpfDifAccXYZ = lpfDifAccX*lpfDifAccX + lpfDifAccY*lpfDifAccY+ lpfDifAccZ*lpfDifAccZ;

        /** Process Gyro Signals**/
        filtHandBpfGyrX.fillBuffer(smpCtr,sensorData.gyr[0], GYRX_BUFFER);
        filtHandBpfGyrY.fillBuffer(smpCtr,sensorData.gyr[1], GYRY_BUFFER);
        filtHandBpfGyrZ.fillBuffer(smpCtr, sensorData.gyr[2], GYRZ_BUFFER);

        bpfGyrX = filtHandBpfGyrX.filterSignal(smpCtr, GYRX_BUFFER, FilterCoefficients.bpf_1_18);
        bpfGyrY = filtHandBpfGyrY.filterSignal(smpCtr, GYRY_BUFFER, FilterCoefficients.bpf_1_18);
        bpfGyrZ = filtHandBpfGyrZ.filterSignal(smpCtr, GYRZ_BUFFER, FilterCoefficients.bpf_1_18);

        sumSqBpfGyrXYZ = bpfGyrX*bpfGyrX + bpfGyrY*bpfGyrY + bpfGyrZ*bpfGyrZ;

        /** Window filtered values**/
        difPreWinHand.fillBuffer(smpCtr, difPre, DIF_PRE_RUN_SUM);
        runSumDifPre = difPreWinHand.calcRunningSum(runSumDifPre, smpCtr, DIF_PRE_RUN_SUM, TWO_AND_A_HALF_SEC);

        lpfDifAccWinHandXYZ.fillBuffer(smpCtr, sumSqLpfDifAccXYZ, LPF_DIF_ACC_XYZ_RUN_SUM);
        runSumSqLpfDifAccXYZ = lpfDifAccWinHandXYZ.calcRunningSum(runSumSqLpfDifAccXYZ, smpCtr, LPF_DIF_ACC_XYZ_RUN_SUM, TWO_AND_A_HALF_SEC);

        bpfGyrWinHandXYZ.fillBuffer(smpCtr, sumSqBpfGyrXYZ, BPF_GYR_XYZ_RUN_SUM);
        runSumSqBpfGyrXYZ = bpfGyrWinHandXYZ.calcRunningSum(runSumSqBpfGyrXYZ, smpCtr, BPF_GYR_XYZ_RUN_SUM, TWO_AND_A_HALF_SEC);

        /** Store Running Sums in buffer to account for delay **/
        difPreFeatHand.fillBuffer(smpCtr, runSumDifPre, DIF_PRE_WIN);
        lpfDifAccXYZFeatHand.fillBuffer(smpCtr,runSumSqLpfDifAccXYZ, SUM_SQ_LPF_DIF_ACC_XYZ_WIN);
        bpfGyrXYZFeatHand.fillBuffer(smpCtr,runSumSqBpfGyrXYZ, SUM_SQ_BPF_GYR_XYZ_WIN);

        if (bDifPreConv) {
            featureCtr++;
//            Log.d(TAG,"featureCtr: "+featureCtr);
            if (featureCtr == TIME_TO_CLASSIFY){
                bClassifyNow = true;
                featureCtr   = 0; //reset the counter
            } else{
                bClassifyNow = false;
            }
        }
        /** Store as vector and classify**/
        if (bClassifyNow){
//            Log.d(TAG, "smpCtr:" + smpCtr);
//            Log.d(TAG,"...ready to classify");
            featSumDifPre         = difPreFeatHand.getFeatureAtIdx(smpCtr, 0,   DIF_PRE_WIN);
            featSumSqLpfDifAccXYZ = lpfDifAccXYZFeatHand.getFeatureAtIdx(smpCtr, 110, SUM_SQ_LPF_DIF_ACC_XYZ_WIN);
            featSumSqBpfGyrXYZ    = bpfGyrXYZFeatHand.getFeatureAtIdx(smpCtr,110, SUM_SQ_BPF_GYR_XYZ_WIN);
            stateIdentified       = classifierBasic.classifyThreeFeaturesPMEAS( featSumDifPre, featSumSqBpfGyrXYZ, featSumSqLpfDifAccXYZ);
//            writeActivityState(stateIdentified);
        }
        /** Increment Counters **/
        smpCtr++;
    }

    public int getStateIdentified(){
        return stateIdentified;
    }

    protected void writeActivityState(int activityState){
////        _activityTime = System.currentTimeMillis();  //time in seconds
////        Movements.toContentValues(_activityTime, activityState, _activityContentValues); //insert
////        _context.getContentResolver().insert(
////                PatientContentProvider.ALL_ACT_RECORDS,
////                _activityContentValues);
//        //TODO:: Consider buffering _activityContentValues and doing a periodic bulk insert
//        // Buffer _activityContentValues into a ContentValues[] object
////        _contentValuesArrayActivities[dbInsertCtr] = _activityContentValues;
//        Log.d(TAG,"adding to insertOperations");
//        insertOperations.add(ContentProviderOperation.newInsert(PatientContentProvider.ALL_ACT_RECORDS)
//                .withValue(MovementSQLiteHelper.COLUMN_DATE_TIME,System.currentTimeMillis())
//                .withValue(MovementSQLiteHelper.COLUMN_ACTIVITY,activityState)
//                .withValue(MovementSQLiteHelper.COLUMN_SYNC,MovementSQLiteHelper.SYNC_STATUS_NEGATIVE)
//                .build());
//        dbInsertCtr++;
//        if (dbInsertCtr == DB_INSERT_CTR_LIMIT-1){
////            _context.getContentResolver().bulkInsert(PatientContentProvider.BULK_INSERT_ACT_RECORDS,_contentValuesArrayActivities);
//            Log.d(TAG,"inserting via applyBatch");
//            try {
//                _context.getContentResolver().applyBatch(PatientContentProvider.AUTHORITY, insertOperations);
//            } catch (RemoteException e) {
//                e.printStackTrace();
//            } catch (OperationApplicationException e) {
//                e.printStackTrace();
//            }
//            insertOperations.clear();
//            dbInsertCtr = 0;
//        }
    }
    /**
     * entry point for debugging values to make sure that they are working
     */
}
