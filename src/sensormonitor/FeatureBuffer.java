package sensormonitor;

/**
 * Created by Michael Del Rosario on 24/05/2018.
 */
public class FeatureBuffer {
    protected int buffIdx;
    protected int featIdx;
    protected double [] buffFeat;
    protected int fixedDelay = 0;

    /**
     *
     * @param buffSize
     */
    public FeatureBuffer(int buffSize){
        buffFeat = new double[buffSize];
    }

    /**
     *
     * @param buffSize
     * @param delayOffset
     */
    public FeatureBuffer(int buffSize, int delayOffset){
        buffFeat = new double[buffSize];
        fixedDelay = delayOffset;
    }

    /**
     *
     * @param ctr
     * @param val
     */
    protected void fill(int ctr, double val){
        buffIdx = ctr & (buffFeat.length-1);
        buffFeat[buffIdx]= val;
    }

    protected double retrieve(int ctr){
        featIdx = (ctr-fixedDelay) & (buffFeat.length-1);
        return buffFeat[featIdx];
    }
}
