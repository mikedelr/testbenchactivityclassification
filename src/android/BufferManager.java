package android;

/**
 * Created by Michael Del Rosario on 12/04/2018.
 */
public class BufferManager {
    protected static void fillBuffer(int ctr, double val, double [] buffer){
        int buffIdx = ctr & (buffer.length-1);
        buffer[buffIdx]= val;
    }

    protected static double getFeatureAtIdx(int ctr, int delayOffset, double[] featureBuffer){
        int featIdx = (ctr-delayOffset) & (featureBuffer.length-1);
        return featureBuffer[featIdx];
    }

    protected static double calcRunningSum(double currentSum, int ctr, double [] windowBuffer, int lengthToSum ){
        double runningSum = currentSum;
        int winIdx =  ctr & (windowBuffer.length-1);
        //add new value to the running sum
        runningSum += windowBuffer[winIdx];
        int oldIdx = winIdx-lengthToSum;
        if (oldIdx < 0){
            oldIdx = windowBuffer.length - lengthToSum + winIdx;
        }
        //subtract the oldest value from the running sum
        runningSum -= windowBuffer[oldIdx];
        return runningSum;
    }

    /** filterSignal **/
    protected static double filterSignal(int signalCnt, double[] signalBuffer, double [] filterCoefficients){
//        Log.d(TAG,"signalBuffer.length: "+signalBuffer.length+ " filterCoefficients.length: "+ filterCoefficients.length);
        double filteredVal = 0; // reset before calculating the new value
        int buffIdx   = signalCnt&(signalBuffer.length-1);
        int fwdLoops  = buffIdx+1;
        int backPtr   = 1;
//        Log.d(TAG,"fwdLoops: "+fwdLoops);
        if (fwdLoops>filterCoefficients.length ){ // If the length of the sensor data buffer is longer than the filter
            fwdLoops=filterCoefficients.length;
        }
        for(int f=0;f<fwdLoops;f++){ // at least once
//            Log.d(TAG,"buffIdx: "+buffIdx+ " f: "+ f);
            filteredVal += signalBuffer[buffIdx] * filterCoefficients[f];
            buffIdx--;
        }
        for(int b=fwdLoops;b<filterCoefficients.length;b++){
//            Log.d(TAG,"signalBuffer.length-backPtr: "+(signalBuffer.length-backPtr)+ " b: "+ b);
            filteredVal += signalBuffer[signalBuffer.length-backPtr]* filterCoefficients[b];
            backPtr++;
        }
        return filteredVal;
    }
}
